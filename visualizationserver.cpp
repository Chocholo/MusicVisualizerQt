#include "visualizationserver.h"
#include "visualizationclient.h"

VisualizationServer::VisualizationServer(QObject *parent) : QTcpServer(parent)
{}

void VisualizationServer::startServer(Player *player)
{
    if (listen(QHostAddress::Any, 1234)) {
        qDebug() << "Started server";
        this->player = player;
    } else {
        qDebug() << "Server: not started";
    }
}

void VisualizationServer::incomingConnection(qintptr handle)
{
    qDebug() << "Mamy polaczenie";
    VisualizationClient *client = new VisualizationClient(this);
    client->setSocket(handle, player);
}
