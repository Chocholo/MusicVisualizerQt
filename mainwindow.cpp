#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent, Player *player) :
    QMainWindow(parent),
    player(player),
    ui(new Ui::MainWindow)
{
    widget = new QWidget;
    setCentralWidget(widget);

    topFiller = new QWidget;
    topFiller->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    prepareInfoLabel();
    prepareBottomFilter();
    prepareLayout();

    createActions();
    createMenus();

    prepareButtons();
    prepareVolumeSlider();
    prepareOffsetSlider();
    preparePlaylistList();
    prepareCronTimers();

    setWindowTitle(tr("Menus"));
    setMinimumSize(1024, 780);
    resize(1024, 780);
    setMaximumSize(1024, 780);
    //ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::createActions()
{
    openFileAction = new QAction(tr("&Open file"), this);
    openFileAction->setStatusTip(tr("Open files to play"));
    connect(openFileAction, &QAction::triggered, this, &MainWindow::openFiles);

    aboutAction = new QAction(tr("&About"), this);
    connect(aboutAction, &QAction::triggered, this, &MainWindow::about);
}

void MainWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(openFileAction);

    fileMenu = menuBar()->addMenu(tr("About"));
    fileMenu->addAction(aboutAction);
}

QFileDialog *MainWindow::prepareFileDialog()
{
    QFileDialog *fileDialog = new QFileDialog(this);
    fileDialog->setNameFilter(tr("Music: (*.wav)"));
    fileDialog->setFileMode(QFileDialog::ExistingFiles);
    return fileDialog;
}

Playlist *MainWindow::preparePlaylistFromFiles(QStringList fileNameList)
{
    Playlist *playlist = new Playlist();
    for(QString fileName : fileNameList) {
        Track *track = new Track(fileName.toStdString());
        if (track->isLoaded() == Track::SONG_LOADED) {
            playlist->addTrack(track);
        }
    }
    return playlist;
}

void MainWindow::preparePlaylistListFromFiles(QListWidget *playlistList, QStringList fileNameList)
{
    playlistList->clear();
    for(QString fileName : fileNameList) {
        QListWidgetItem *fileItem =new QListWidgetItem();
        fileItem->setText(fileName);
        playlistList->insertItem(playlistList->currentRow(), fileItem);
    }
}

void MainWindow::prepareInfoLabel()
{
    infoLabel = new QLabel(tr("<i>Choose a menu option, or right-click to "
                              "invoke a context menu</i>"));
    infoLabel->setFrameStyle(QFrame::StyledPanel | QFrame::Sunken);
    infoLabel->setAlignment(Qt::AlignCenter);
    connect(player, SIGNAL(newSongStarted(std::string)), this, SLOT(updateInfoLabel(std::string)));
}

void MainWindow::prepareBottomFilter()
{
    bottomFiller = new QWidget;
    bottomFiller->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}

void MainWindow::prepareLayout()
{
    QVBoxLayout *layout = new QVBoxLayout;
    layout->setMargin(5);
    layout->addWidget(topFiller);
    layout->addWidget(infoLabel);
    layout->addWidget(bottomFiller);
    widget->setLayout(layout);
}

void MainWindow::prepareButtons()
{
    preparePlayButton();
    preparePauseButton();
    prepareNextButton();
    preparePreviousButton();
}

void MainWindow::preparePlayButton()
{
    playButton = new QPushButton("&Play", widget);
    playButton->move(200, 400);
    connect(playButton, SIGNAL(released()), this, SLOT(play()));
}

void MainWindow::preparePauseButton()
{
    pauseButton = new QPushButton("&Pause", widget);
    pauseButton->move(300, 400);
    connect(pauseButton, SIGNAL(released()), this, SLOT(pause()));
}

void MainWindow::prepareNextButton()
{
    nextButton = new QPushButton("&Next", widget);
    nextButton->move(400, 400);
    connect(nextButton, SIGNAL(released()), this, SLOT(next()));
}

void MainWindow::preparePreviousButton()
{
    previousButton = new QPushButton("&Previous", widget);
    previousButton->move(500, 400);
    previousButton->setMinimumWidth(150);
    connect(previousButton, SIGNAL(released()), this, SLOT(previous()));
}

void MainWindow::prepareVolumeSlider()
{
    volumeSlider = new QSlider(widget);
    volumeSlider->setMinimum(0);
    volumeSlider->setMaximum(100);
    volumeSlider->setSingleStep(1);
    volumeSlider->setTickInterval(1);
    volumeSlider->move(50, 500);
    volumeSlider->setMinimumSize(200, 200);
    volumeSlider->setOrientation(Qt::Orientation::Horizontal);
    volumeSlider->setValue(player->getVolume());
    connect(volumeSlider, SIGNAL(valueChanged(int)), this, SLOT(changeVolume()));
}

void MainWindow::prepareOffsetSlider()
{
    offsetSlider = new QSlider(widget);
    offsetSlider->setMinimum(0);
    offsetSlider->setMaximum(100);
    offsetSlider->setSingleStep(1);
    offsetSlider->setTickInterval(1);
    offsetSlider->setOrientation(Qt::Orientation::Horizontal);
    offsetSlider->move(270, 500);
    offsetSlider->setMinimumSize(400, 200);
    connect(offsetSlider, SIGNAL(valueChanged(int)), this, SLOT(changeOffset()));
}

void MainWindow::preparePlaylistList()
{
    playlistList = new QListWidget(this);
    playlistList->move(700, 50);
    playlistList->setMinimumSize(300, 670);
    connect(playlistList, SIGNAL(itemDoubleClicked(QListWidgetItem*)),
            this, SLOT(selectTrackFromPlayList(QListWidgetItem*)));
}

void MainWindow::prepareCronTimers()
{
    prepareOffsetCronTimer();
    prepareNextInPlaylistCronTimer();
    prepareSendStreamToVisualizersCronTimer();
}

void MainWindow::prepareOffsetCronTimer()
{
    offsetCronTimer = new QTimer(this);
    connect(offsetCronTimer, SIGNAL(timeout()), this, SLOT(timerUpdateoffset()));
    offsetCronTimer->start(1000);
}

void MainWindow::prepareNextInPlaylistCronTimer()
{
    nextInPlaylistCronTimer = new QTimer(this);
    connect(nextInPlaylistCronTimer, SIGNAL(timeout()), this, SLOT(timerNextIfTrackEnded()));
    nextInPlaylistCronTimer->start(1000);
}

void MainWindow::prepareSendStreamToVisualizersCronTimer()
{
    sendStreamToVisualizersCronTimer = new QTimer(this);
    connect(sendStreamToVisualizersCronTimer, SIGNAL(timeout()), this, SLOT(timerSendStreamToVisualizers()));
    sendStreamToVisualizersCronTimer->start(50);
}

void MainWindow::openFiles()
{
    QFileDialog *openFilesDialog = prepareFileDialog();
    if (openFilesDialog->exec()) {
        QStringList selectedFiles = openFilesDialog->selectedFiles();
        Playlist *playlistFromSelectedFiles = preparePlaylistFromFiles(selectedFiles);
        player->setPlaylist(playlistFromSelectedFiles);
        preparePlaylistListFromFiles(playlistList, selectedFiles);
    }
    delete openFilesDialog;
}

void MainWindow::about()
{
    QMessageBox::information(this, tr("About"), tr("About x"), QMessageBox::Ok);
}

void MainWindow::play()
{
    player->play();
}

void MainWindow::pause()
{
    player->pause();
}

void MainWindow::next()
{
    player->next();
}

void MainWindow::previous()
{
    player->previous();
}

void MainWindow::changeVolume()
{
    player->setVolume(volumeSlider->value());
}

void MainWindow::changeOffset()
{
    if(player->isTrackSelected()) {
        player->setOffset(offsetSlider->value(), offsetSlider->maximum());
    } else {
        offsetSlider->setValue(0);
    }
}

void MainWindow::timerUpdateoffset()
{
    if (player->isTrackSelected()) {
        offsetSlider->blockSignals(true);
        offsetSlider->setValue(player->getOffsetNormalized());
        offsetSlider->blockSignals(false);
    }
}

void MainWindow::timerNextIfTrackEnded()
{
    if (player->hasCurrentSongEnded()) {
        next();
    }
}

void MainWindow::timerSendStreamToVisualizers()
{
    player->emitSamplesToSend();
}

void MainWindow::selectTrackFromPlayList(QListWidgetItem *selectedItem)
{
    for (int i=0; i<playlistList->count();i++) {
        QListWidgetItem *item = playlistList->item(i);
        if (selectedItem == item) {
            player->selectSong(i);
            player->play();
            break;
        }
    }
}

void MainWindow::updateInfoLabel(std::string songName)
{
    infoLabel->setText(QString::fromStdString(songName));
}
