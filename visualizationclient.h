#ifndef VISUALIZATIONCLIENT_H
#define VISUALIZATIONCLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <aquila.h>
#include "player.h"
#include "signalsbucket.h"

class VisualizationClient : public QObject
{
    Q_OBJECT
public:
    explicit VisualizationClient(QObject *parent = nullptr);
    void setSocket(int description, Player *player);

signals:

public slots:
    void connected();
    void disconnected();
    void readySend(SignalsBucket signalsBucket);

private:
    QTcpSocket *socket;
    Player *player;
};

#endif // VISUALIZATIONCLIENT_H
