#include "player.h"

Player::Player()
{
    volume = 50;
    currentlyPlaying = nullptr;
    playlist = nullptr;
}

Player::~Player()
{
    if (playlist != nullptr) {
        delete playlist;
    }
}

void Player::setPlaylist(Playlist *value)
{
    if (playlist != nullptr) {
        delete playlist;
    }
    playlist = value;
}

bool Player::play()
{
    if (isTrackSelected()) {
        currentlyPlaying->stop();
    }
    if (playlist != nullptr) {
        currentlyPlaying = playlist->getCurrentTrack();
        if (currentlyPlaying != nullptr) {
            currentlyPlaying->setVolume(volume);
            playing = true;
            if (currentlyPlaying->play() == Track::SONG_PLAYING) {
                emitNewSongStarted(currentlyPlaying->getName());
            }
        }
    }
    return Track::SONG_NOT_PLAYING;
}

void Player::pause()
{
    if (playlist != nullptr) {
        currentlyPlaying = playlist->getCurrentTrack();
        if (isTrackSelected()) {
            currentlyPlaying->pause();
        }

    }
}

bool Player::next()
{
    if (playlist != nullptr) {
        Track *currentTrack = playlist->getCurrentTrack();
        if (isTrackSelected()) {
            currentTrack->stop();
            currentlyPlaying = playlist->nextTrack();
            resetCurrentlyPlayingSong();
            return currentlyPlaying->play();
        }
    }
    return Track::SONG_NOT_PLAYING;
}

bool Player::previous()
{
    if (playlist != nullptr) {
        Track *currentTrack = playlist->getCurrentTrack();
        if (isTrackSelected()) {
            currentTrack->stop();
            currentlyPlaying = playlist->previousTrack();
            resetCurrentlyPlayingSong();
            return currentlyPlaying->play();
        }
    }
    return Track::SONG_NOT_PLAYING;
}

int Player::getVolume() const
{
    return volume;
}

void Player::setVolume(int value)
{
    if (volume >= 0 && volume <= 100) {
        volume = value;
        if (currentlyPlaying != nullptr) {
            currentlyPlaying->setVolume(volume);
        }
    }
}

int Player::getOffsetNormalized()
{
    if (isTrackSelected()) {
        sf::Time duration = currentlyPlaying->getDuration();
        sf::Time offset = currentlyPlaying->getOffset();
        float offsetNormalized = offset.asSeconds() / duration.asSeconds();
        return offsetNormalized * 100;
    }
    return 0;
}

int Player::getOffsetInMiliseconds()
{
    if (currentlyPlaying != nullptr) {
        return currentlyPlaying->getOffset().asMilliseconds();
    }
    return -1;
}

void Player::setOffset(int value, int max)
{
    if (isTrackSelected()) {
        float valueNormalized = (float)value / (float)max;
        float duration = currentlyPlaying->getDuration().asMilliseconds();
        int offset = valueNormalized * duration;
        currentlyPlaying->setOffset(sf::Time(sf::milliseconds(offset)));
    }
}

bool Player::hasCurrentSongEnded()
{
    if (isTrackSelected() && playing) {
        return currentlyPlaying->getStatus() == sf::Music::Stopped;
    } else {
        return CURRENT_SONG_NOT_ENDED;
    }
}

bool Player::isTrackSelected()
{
    return currentlyPlaying != nullptr;
}

void Player::selectSong(int songNo)
{
    playlist->setPosition(songNo);
}

sf::Time Player::getCurrentTrackDuration()
{
    if (isTrackSelected()) {
        return currentlyPlaying->getDuration();
    }
    return sf::Time(sf::seconds(-1));
}

void Player::emitSamplesToSend()
{
    if (currentlyPlaying != nullptr) {
        int N = 3272;
        int roof = currentlyPlaying->getSampleCount();
        SignalsBucket signalsBucket;
        int framePointer = currentlyPlaying->getOffset().asMilliseconds() * 1.0f * (currentlyPlaying->getSampleRate() / 1000.0);
        for (int i = framePointer, j = 0; i < (framePointer + N)
            && framePointer < roof - N; i++, j++) {
            signalsBucket.samples[j] = currentlyPlaying->getSample(i);
        }
        emit samplesToSend(signalsBucket);
    } else {
        emit samplesToSend(SignalsBucket());
    }
}

void Player::emitNewSongStarted(std::string songName)
{
    emit newSongStarted(songName);
}

void Player::resetCurrentlyPlayingSong()
{
    currentlyPlaying->setVolume(volume);
    currentlyPlaying->setOffset(sf::Time(sf::seconds(0)));
}
