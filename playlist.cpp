#include "playlist.h"

Playlist::Playlist()
{
    currentTrack = 0;
}

Playlist::~Playlist()
{
    for(auto track : tracks) {
        delete track;
    }
    tracks.clear();
}

void Playlist::shuffle()
{
    std::random_shuffle(tracks.begin(), tracks.end());
}

void Playlist::sort()
{
    std::sort(tracks.begin(), tracks.end(),
              [](Track *a, Track *b) {return a->getName() > b->getName();});
}

void Playlist::addTrack(Track *track)
{
    tracks.push_back(track);
}

void Playlist::removeTrack(Track *track)
{
    // TODO, update currentPosition
    //tracks.
    //delete track;
}

void Playlist::setPosition(int position)
{
    if (position < 0 || position == 0) {
        currentTrack = 0;
    } else if (position >= tracks.size()) {
        currentTrack = tracks.size() - 1;
    } else {
        currentTrack = position;
    }
}

Track * Playlist::getCurrentTrack()
{
    if (tracks.size() != 0) {
        return tracks.at(currentTrack);
    }
    return nullptr;
}

Track *Playlist::nextTrack()
{
    setPosition(currentTrack + 1);
    return getCurrentTrack();
}

Track *Playlist::previousTrack()
{
    setPosition(currentTrack - 1);
    return getCurrentTrack();
}
