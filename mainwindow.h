#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QBoxLayout>
#include <QFileDialog>
#include <QFile>
#include <QMessageBox>
#include <QPushButton>
#include <QMessageLogger>
#include <QListWidget>
#include <QSize>
#include <QSlider>
#include <QTimer>
#include <string>
#include "track.h"
#include "player.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0, Player *player = nullptr);
    ~MainWindow();

signals:
    void sendAquilaSamples(Aquila::SampleType samples[3273]);

private:
    void createActions();
    void createMenus();

    Ui::MainWindow *ui;

    QWidget *widget;
    QWidget *topFiller;
    QWidget *bottomFiller;

    QMenu *fileMenu;
    QAction *openFileAction;

    QMenu *aboutMenu;
    QAction *aboutAction;

    QLabel *infoLabel;

    QFileDialog *prepareFileDialog();

    Player *player;
    Playlist *preparePlaylistFromFiles(QStringList fileNameList);

    QPushButton *playButton;
    QPushButton *pauseButton;
    QPushButton *nextButton;
    QPushButton *previousButton;

    QSlider *volumeSlider;
    QSlider *offsetSlider;

    QTimer *offsetCronTimer;
    QTimer *nextInPlaylistCronTimer;
    QTimer *sendStreamToVisualizersCronTimer;

    QListWidget *playlistList;

    void preparePlaylistListFromFiles(QListWidget *playlistList, QStringList fileNameList);
    void prepareInfoLabel();
    void prepareBottomFilter();
    void prepareLayout();
    void prepareButtons();
    void preparePlayButton();
    void preparePauseButton();
    void prepareNextButton();
    void preparePreviousButton();
    void prepareVolumeSlider();
    void prepareOffsetSlider();
    void preparePlaylistList();
    void prepareCronTimers();
    void prepareOffsetCronTimer();
    void prepareNextInPlaylistCronTimer();
    void prepareSendStreamToVisualizersCronTimer();
private slots:
    void openFiles();
    void about();
    void play();
    void pause();
    void next();
    void previous();
    void changeVolume();
    void changeOffset();
    void timerUpdateoffset();
    void timerNextIfTrackEnded();
    void timerSendStreamToVisualizers();
    void selectTrackFromPlayList(QListWidgetItem *selectedItem);
    void updateInfoLabel(std::string songName);
};

#endif // MAINWINDOW_H
