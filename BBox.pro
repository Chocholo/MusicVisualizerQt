#-------------------------------------------------
#
# Project created by QtCreator 2018-03-30T12:44:09
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BBox
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


###################SFML
CONFIG += c++11
LIBS += -LC:/Users/Chocholo/Documents/BBox/SFML/lib
CONFIG(release, debug|release): LIBS += -lsfml-audio -lsfml-graphics -lsfml-main -lsfml-network -lsfml-window -lsfml-system
CONFIG(debug, debug|release): LIBS +=  -lsfml-audio -lsfml-graphics -lsfml-main -lsfml-network -lsfml-window -lsfml-system
INCLUDEPATH += C:/Users/Chocholo/Documents/BBox/SFML/include
DEPENDPATH += C:/Users/Chocholo/Documents/BBox/SFML/include
###################

###################AQUILA
CONFIG += c++11
LIBS += -LC:/Users/Chocholo/Documents/BBox/Aquila
CONFIG(release, debug|release): LIBS += -lAquila -lOoura_fft
CONFIG(debug, debug|release): LIBS += -lAquila -lOoura_fft
INCLUDEPATH += C:/Users/Chocholo/Documents/BBox/Aquila
DEPENDPATH += C:/Users/Chocholo/Documents/BBox/Aquila
###################


################### KISS FFT
INCLUDEPATH += C:/Users/Chocholo/Documents/BBox/kiss_fft
#############################


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    track.cpp \
    player.cpp \
    playlist.cpp \
    visualizationserver.cpp \
    visualizationclient.cpp \
    signalsbucket.cpp

HEADERS += \
        mainwindow.h \
    track.h \
    player.h \
    playlist.h \
    visualizationserver.h \
    visualizationclient.h \
    signalsbucket.h

FORMS += \
        mainwindow.ui
