#include "track.h"

Track::Track(std::string fileName)
{
    if(trackFile.openFromFile(fileName)) {
        trackFileAquila = new Aquila::WaveFile(fileName);
        loaded = true;
    } else {
        trackFileAquila = nullptr;
        loaded = false;
    }
}

Track::~Track()
{
    delete trackFileAquila;
}

std::string Track::getName() const
{
    if (loaded) {
        std::string fullFilename = trackFileAquila->getFilename();
        std::size_t lastOccurence = fullFilename.find_last_of("/");
        if (lastOccurence != -1) {
            return fullFilename.substr(lastOccurence+1);
        } else {
            return fullFilename;
        }
    } else return "";
}

bool Track::play()
{
    if (loaded) {
        trackFile.play();
        return SONG_PLAYING;
    }
    return SONG_NOT_PLAYING;
}

void Track::pause()
{
    if (loaded) {
        trackFile.pause();
    }
}

void Track::stop()
{
    if (loaded) {
        trackFile.stop();
    }
}

sf::Time Track::getDuration() const
{
    if (loaded) {
        return trackFile.getDuration();
    }
    return sf::Time(sf::seconds(-1));
}

sf::SoundSource::Status Track::getStatus() const
{
    if (loaded) {
        return trackFile.getStatus();
    }
    return sf::SoundSource::Status::Stopped;
}

sf::Time Track::getOffset() const
{
    if (loaded) {
        return trackFile.getPlayingOffset();
    }
    return sf::Time(sf::seconds(-1));
}

unsigned int Track::getSampleRate() const
{
    if (loaded) {
        return trackFile.getSampleRate();
    }
    return -1;
}

unsigned int Track::getSampleCount() const
{
    if (loaded) {
        return trackFileAquila->getSamplesCount();
    }
    return -1;
}

Aquila::SampleType Track::getSample(int i) const
{
    if (loaded) {
        return trackFileAquila->sample(i);
    }
    return -1;
}

void Track::setOffset(sf::Time offset)
{
    if (loaded) {
        trackFile.setPlayingOffset(offset);
    }
}

bool Track::isLoaded()
{
    return loaded;
}

void Track::setVolume(int volume)
{
    if (loaded) {
        trackFile.setVolume(volume);
    }
}
