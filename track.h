#ifndef TRACK_H
#define TRACK_H
#include <string>
#include <SFML/Audio.hpp>
#include <aquila.h>

class Track
{
public:
    const static bool SONG_LOADED = true;
    const static bool SONG_NOT_LOADED = false;
    const static bool SONG_PLAYING = true;
    const static bool SONG_NOT_PLAYING = false;

    Track(std::string fileName);
    ~Track();

    sf::Time getOffset() const;
    void setOffset(sf::Time offset);

    bool isLoaded();

    std::string getName() const;

    sf::Time getDuration() const;

    sf::SoundSource::Status getStatus() const;

    unsigned int getSampleRate() const;

    unsigned int getSampleCount() const;

    Aquila::SampleType getSample(int i) const;

    void setVolume(int volume);

    bool play();
    void pause();
    void stop();

private:
    sf::Music trackFile;
    Aquila::WaveFile *trackFileAquila;
    bool loaded;
};

#endif // TRACK_H
