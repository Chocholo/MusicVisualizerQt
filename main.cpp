#include "mainwindow.h"
#include <SFML/Audio.hpp>
#include <QApplication>
#include "track.h"
#include "player.h"
#include "visualizationserver.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    Player *player = new Player();
    VisualizationServer *visualizationServer = new VisualizationServer(0);
    visualizationServer->startServer(player);
    MainWindow w(0, player);
    w.show();
    return a.exec();
}
