#ifndef PLAYER_H
#define PLAYER_H
#include <QObject>
#include <vector>
#include "track.h"
#include "playlist.h"
#include "signalsbucket.h"
#include <aquila.h>

class Player : public QObject
{
    Q_OBJECT
public:
    const static bool CURRENT_SONG_ENDED = true;
    const static bool CURRENT_SONG_NOT_ENDED = false;

    Player();
    ~Player();

    void setPlaylist(Playlist *value);
    bool play();
    void pause();
    bool next();
    bool previous();
    int getVolume() const;
    void setVolume(int value);
    int getOffsetNormalized();
    int getOffsetInMiliseconds();
    void setOffset(int value, int max);
    bool hasCurrentSongEnded();
    bool isTrackSelected();
    void selectSong(int songNo);
    sf::Time getCurrentTrackDuration();
    void emitSamplesToSend();
    void emitNewSongStarted(std::string songName);

signals:
    void samplesToSend(SignalsBucket);
    void newSongStarted(std::string songName);

private:
    Playlist *playlist;
    bool playing;
    Track *currentlyPlaying;
    void resetCurrentlyPlayingSong();
    int volume;
};

#endif // PLAYER_H
