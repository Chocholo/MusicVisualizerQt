#include "visualizationclient.h"

VisualizationClient::VisualizationClient(QObject *parent) : QObject(parent)
{

}

void VisualizationClient::setSocket(int description, Player *player)
{
    socket = new QTcpSocket(this);
    socket->setSocketDescriptor(description);
    this->player = player;

    connect(socket, SIGNAL(connected()), this, SLOT(connected()));
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()));
    connect(player, SIGNAL(samplesToSend(SignalsBucket)), this, SLOT(readySend(SignalsBucket)));
}

void VisualizationClient::connected()
{
    qDebug() << "Connected";
}

void VisualizationClient::disconnected()
{
    qDebug() << "Disconected";
}

void VisualizationClient::readySend(SignalsBucket signalsBucket)
{
    socket->write((const char *)signalsBucket.samples, sizeof(Aquila::SampleType)*3287);
}
