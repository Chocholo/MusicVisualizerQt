#ifndef VISUALIZATIONSERVER_H
#define VISUALIZATIONSERVER_H
#include "player.h"
#include <QTcpServer>
#include <QTcpSocket>
#include <QAbstractSocket>

class VisualizationServer : public QTcpServer
{
public:
    VisualizationServer(QObject *parent);
    void startServer(Player *player);
protected:
    void incomingConnection(qintptr handle);
private:
    Player *player;
};

#endif // VISUALIZATIONSERVER_H
