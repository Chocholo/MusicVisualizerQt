#ifndef PLAYLIST_H
#define PLAYLIST_H
#include "track.h"
#include <list>
#include <vector>
#include <algorithm>
#include <deque>
#include <random>
class Playlist
{
private:
    std::vector<Track *> tracks;
    int currentTrack;
public:
    Playlist();
    ~Playlist();
    void shuffle();
    void sort();
    void addTrack(Track *track);
    void removeTrack(Track *track);
    void setPosition(int position);
    Track *getCurrentTrack();
    Track *nextTrack();
    Track *previousTrack();
};

#endif // PLAYLIST_H
